/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pattraporn.dbpj.model;

import com.pattraporn.dbpj.dao.productDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;





/**
 *
 * @author werapan
 */
public class Orders_Detail {

    private int id;
    product product;
    private String productName;
    private double productPrice;
    private int qty;
    private Orders orderDetail;

    public Orders_Detail(int id, product product, String productName, double productPrice, int qty, Orders orderDetail) {
        this.id = id;
        this.product = product;
        this.productName = productName;
        this.productPrice = productPrice;
        this.qty = qty;
        this.orderDetail = orderDetail;
    }

    public Orders_Detail(product product, String productName, double productPrice, int qty) {
        this.product = product;
        this.productName = productName;
        this.productPrice = productPrice;
        this.qty = qty;
    }

    public Orders_Detail() {
        this.id = -1;
    }

    public Orders_Detail(product product, String productName, double productPrice, int qty, Orders orderDetail) {
        this.product = product;
        this.productName = productName;
        this.productPrice = productPrice;
        this.qty = qty;
        this.orderDetail = orderDetail;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public product getproduct() {
        return product;
    }

    public void setproduct(product product) {
        this.product = product;
    }

    public String getproductName() {
        return productName;
    }

    public void setproductName(String productName) {
        this.productName = productName;
    }

    public double getproductPrice() {
        return productPrice;
    }

    public void setproductPrice(double productPrice) {
        this.productPrice = productPrice;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public Orders getOrder() {
        return orderDetail;
    }

    public void setOrder(Orders orderDetail) {
        this.orderDetail = orderDetail;
    }

    @Override
    public String toString() {
        return "Orders_Detail{" + "id=" + id + ", product=" + product + ", productName=" + productName + ", productPrice=" + productPrice + ", qty=" + qty + "}";
    }

    public double getTotal() {
        return qty * productPrice;
    }

    public static Orders_Detail fromRS(ResultSet rs) {
        Orders_Detail orderDetail = new Orders_Detail();
        productDao productDao = new productDao();
        try {
            orderDetail.setId(rs.getInt("order_detail_id"));
            int productId = rs.getInt("product_id");
            product item = productDao.get(productId);
            orderDetail.setproduct(item);
            orderDetail.setQty(rs.getInt("qty"));
            orderDetail.setproductName(rs.getString("product_name"));
            orderDetail.setproductPrice(rs.getDouble("product_price"));
            

        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        }
        return orderDetail;
    }
}
